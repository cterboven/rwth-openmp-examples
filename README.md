# README #

This is a collection of small OpenMP-parallel codes serving as examples in lectures and workshop. All codes are released under the Creative Commons Attribution-ShareAlike 4.0 International License.

01-HelloWorld: This is a simple OpenMP-parallel "hello world" program. It shows how the output may be mixed up without proper synchronization. The orphaned version illustrates how function calls can be made from within a parallel region.

02-VectorAddition: This program performs the linear algebra operation A = B + C for vectors A, B, C using the for worksharing construct. All vectors are initialized in parallel to achive good performance through memory placement on NUMA machines (assuming that thread binding is enabled).

03-SingleInputOutput: Slightly modified version of 02-VectorAddition in which the single construct is used to allocate the arrays from within a parallel region and also to limited the console output to one thread only.

04-Fibonacci: Recursive computation of the Fibonacci numbers using the OpenMP tasking concept. The code contains four different versions (v0 to v3) which improve performance and scalability in every step. V0: naive implementation with high overhead. V1: reduction of the overhead by means of the if clause (cut-off). V2: further reduction of the overhead by means of a manual cut-off to avoid any OpenMP constructs. V4: only one task construct used to parallelize the recursion.

05-Pi: Approximation of Pi by means of simple numerical integration. The code illustrates the need for privatization to avoid a data races and the use of the reduction clause.

06-Sudoku: Brute force Sudoku solver employing the OpenMP tasking concept and a somewhat hacky cut-off strategy. Some example sudokus are included. If you have an idea for a better cut-off strategy please let us know.

We are about to add more of our codes...