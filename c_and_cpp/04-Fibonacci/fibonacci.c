/*
 *  OpenMP example collection from RWTH Aachen University
 * 
 *  Written and maintained in 2011-2015 by
 *     Christian Terboven <terboven@itc.rwth-aachen.de>, RWTH Aachen University
 *  and
 *     Dirk Schmidl <schmidl@itc.rwth-aachen.de>, RWTH Aachen University
 *
 *  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 *  http://creativecommons.org/licenses/by-sa/4.0/
 */

#include <stdio.h>

#include <omp.h>



int fib_v0(int n)
{
  int x, y;
  if (n < 2) 
	  return n;

#pragma omp task shared(x)
  x = fib_v0(n - 1);

#pragma omp task shared(y)
  y = fib_v0(n - 2);

#pragma omp taskwait
  return x+y;

}


int fib_v1(int n)
{
  int x, y;
  if (n < 2) 
	  return n;

#pragma omp task shared(x) if(n >= 30)
  x = fib_v1(n - 1);

#pragma omp task shared(y) if(n >= 30)
  y = fib_v1(n - 2);

#pragma omp taskwait
  return x+y;

}


int fib_v2(int n)
{
  int x, y;
  if (n < 2) 
	  return n;
  if (n < 30)
	  return fib_v2(n-1) + fib_v2(n-2);

#pragma omp task shared(x)
  x = fib_v2(n - 1);

#pragma omp task shared(y)
  y = fib_v2(n - 2);

#pragma omp taskwait
  return x+y;

}


int fib_v3(int n)
{
  int x, y;
  if (n < 2) 
	  return n;
  if (n < 30)
	  return fib_v3(n-1) + fib_v3(n-2);

#pragma omp task shared(x)
  x = fib_v3(n - 1);

  y = fib_v3(n - 2);

#pragma omp taskwait
  return x+y;

}


int main()
{
  int n,fibonacci, nthreads;
  double starttime;
  double reftime, speedup;
  printf("Please insert n, to calculate fib(n): \n");
  scanf("%d",&n);

  /// v0 ///
  printf("\n");

  starttime=omp_get_wtime();
#pragma omp parallel
  {
#pragma omp single
	  {
		  nthreads = omp_get_num_threads();
		  fibonacci=fib_v0(n);
	  }
  }
  starttime = omp_get_wtime() - starttime;
  reftime = starttime;
  speedup = reftime / starttime;
  printf("fib(%d)=%d \n", n, fibonacci);
  printf("calculation (v0) took %2.2lf sec with %d threads (speedup: %1.2lf)\n", starttime, nthreads, speedup);

  /// v1 ///
  printf("\n");

  starttime=omp_get_wtime();
#pragma omp parallel
  {
#pragma omp single
	  {
		  nthreads = omp_get_num_threads();
		  fibonacci=fib_v1(n);
	  }
  }
  starttime = omp_get_wtime() - starttime;
  reftime = starttime;
  speedup = reftime / starttime;
  printf("fib(%d)=%d \n", n, fibonacci);
  printf("calculation (v1) took %2.2lf sec with %d threads (speedup: %1.2lf)\n", starttime, nthreads, speedup);

  /// v2 ///
  printf("\n");

  starttime=omp_get_wtime();
#pragma omp parallel
  {
#pragma omp single
	  {
		  nthreads = omp_get_num_threads();
		  fibonacci=fib_v2(n);
	  }
  }
  starttime = omp_get_wtime() - starttime;
  reftime = starttime;
  speedup = reftime / starttime;
  printf("fib(%d)=%d \n", n, fibonacci);
  printf("calculation (v2) took %2.2lf sec with %d threads (speedup: %1.2lf)\n", starttime, nthreads, speedup);

   /// v3 ///
  printf("\n");

  starttime=omp_get_wtime();
#pragma omp parallel
  {
#pragma omp single
	  {
		  nthreads = omp_get_num_threads();
		  fibonacci=fib_v3(n);
	  }
  }
  starttime = omp_get_wtime() - starttime;
  reftime = starttime;
  speedup = reftime / starttime;
  printf("fib(%d)=%d \n", n, fibonacci);
  printf("calculation (v3) took %2.2lf sec with %d threads (speedup: %1.2lf)\n", starttime, nthreads, speedup);

  /// done ///

  return 0;
}
