/*
 *  OpenMP example collection from RWTH Aachen University
 * 
 *  Written and maintained in 2011-2015 by
 *     Christian Terboven <terboven@itc.rwth-aachen.de>, RWTH Aachen University
 *  and
 *     Dirk Schmidl <schmidl@itc.rwth-aachen.de>, RWTH Aachen University
 *
 *  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 *  http://creativecommons.org/licenses/by-sa/4.0/
 */

#include "SudokuBoard.h"

#include <cstring>

CSudokuBoard::CSudokuBoard(int fsize, int bsize)
	: field_size(fsize), block_size(bsize), solutions(-1)
{
#if USE_STD_VECTOR
	field.resize(field_size);
	for (int i = 0; i < field_size; i++) {
		field[i].resize(field_size);
	}
#else
	field = new int[field_size*field_size];
#endif
}


CSudokuBoard::CSudokuBoard(const CSudokuBoard& other)
	: field_size(other.getFieldSize()), block_size(other.getBlockSize()), solutions(other.getNumSolutions())
#if USE_STD_VECTOR
	, field(other.field)
#endif
{
#if ! USE_STD_VECTOR
	field = new int[field_size*field_size];
	std::memcpy(field, other.field, sizeof(int) * field_size*field_size);
#endif
}


CSudokuBoard::~CSudokuBoard(void)
{
#if ! USE_STD_VECTOR	
	delete[] field;
#endif
}


bool CSudokuBoard::loadFromFile(char *filename)
{
  std::ifstream ifile(filename);
  
  if (!ifile) {
    std::cout << "There was an error opening the input file " << filename << std::endl;
    std::cout << std::endl;
    return false;
  }

  for (int i = 0; i < this->field_size; i++) {
    for (int j = 0; j < this->field_size; j++) {
#if USE_STD_VECTOR
      ifile >> this->field[i][j];
#else
	  ifile >> this->field[ACCESS(i,j)];
#endif
    }
  }

  return true;
}


void CSudokuBoard::printBoard()
{
	for(int i = 0; i < field_size; i++) {
		for(int j = 0; j < field_size; j++) {
			std::cout << std::setw(3) << 
#if USE_STD_VECTOR
				this->field[i][j] 
#else			
				this->field[ACCESS(i,j)] 
#endif
				<< " ";
		}
		std::cout << std::endl;
	}
}


bool CSudokuBoard::check(int x, int y, int value)
{
	if(checkHorizontal(y, value))
		return true;
	if(checkVertical(x, value))
		return true;
	if(checkBox(x, y, value))
		return true;
	return false;
}


bool CSudokuBoard::checkHorizontal(int y, int value) {
	for(int i = 0; i < field_size; i++)
#if USE_STD_VECTOR
		if(field[y][i] == value)
#else
		if(field[ACCESS(y,i)] == value)
#endif
			return true;
	return false;
}


bool CSudokuBoard::checkVertical(int x, int value) {
	for(int i = 0; i < field_size; i++)
#if USE_STD_VECTOR	
		if(field[i][x] == value)
#else
		if(field[ACCESS(i,x)] == value)
#endif
			return true;
	return false;
}


bool CSudokuBoard::checkBox(int x, int y, int value) {
	// find suitable box edge
	int x_box = (int)(x / block_size) * block_size;
	int y_box = (int)(y / block_size) * block_size;

	for(int i = y_box; i < y_box + block_size; i++)
		for(int j = x_box; j < x_box + block_size; j++)
#if USE_STD_VECTOR
			if(field[i][j] == value)
#else
			if(field[ACCESS(i,j)] == value)
#endif
				return true;
  
	return false;
}
