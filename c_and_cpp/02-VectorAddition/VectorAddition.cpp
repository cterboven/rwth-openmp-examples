/*
 *  OpenMP example collection from RWTH Aachen University
 * 
 *  Written and maintained in 2011-2015 by
 *     Christian Terboven <terboven@itc.rwth-aachen.de>, RWTH Aachen University
 *  and
 *     Dirk Schmidl <schmidl@itc.rwth-aachen.de>, RWTH Aachen University
 *
 *  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 *  http://creativecommons.org/licenses/by-sa/4.0/
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <omp.h>



int main(int argc, char* argv[])
{
	const int iDimension = 230420230;

	double *A = new double[iDimension], *B = new double[iDimension], *C = new double[iDimension];

#pragma omp parallel for
	for (int i = 0; i < iDimension; i++)
	{
		A[i] = 0.0;
		B[i] = 1.0;
		C[i] = 2.0; 
	}

	double t1 = omp_get_wtime();
#pragma omp parallel
	{
	
#pragma omp single
	{
		std::cout << "Computation started with " << omp_get_num_threads() << " threads ..." << std::endl;
		fflush(stdout);
	}
	
#pragma omp for
		for (int i = 0; i < iDimension; i++)
		{
			A[i] = B[i] + C[i];
		}
	} // end omp parallel
	double t2 = omp_get_wtime() - t1;

	std::cout << "Computation took " << t2 << " seconds." << std::endl;

	delete[] A, B, C;

	return 0;
}
