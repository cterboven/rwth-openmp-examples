/*
 *  OpenMP example collection from RWTH Aachen University
 * 
 *  Written and maintained in 2011-2015 by
 *     Christian Terboven <terboven@itc.rwth-aachen.de>, RWTH Aachen University
 *  and
 *     Dirk Schmidl <schmidl@itc.rwth-aachen.de>, RWTH Aachen University
 *
 *  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 *  http://creativecommons.org/licenses/by-sa/4.0/
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include <omp.h>



int main(int argc, char* argv[])
{
	size_t stDimension = 0;
	double *A, *B, *C;

	std::cout << "Computation will be performed with " << omp_get_max_threads() << " threads ..." << std::endl;
	fflush(stdout);

#pragma omp parallel shared(stDimension, A, B, C)
{

// only one thread will query input size and perform allocation
#pragma omp single
{
	std::cout << "Please enter array dimension: ";
	std::cin >> stDimension;
	
	A = new double[stDimension];
	B = new double[stDimension];
	C = new double[stDimension];
}

// initialization will be done in parallel
#pragma omp for nowait
	for (size_t i = 0; i < stDimension; i++)
	{
		A[i] = 0.0;
	}
#pragma omp for nowait
	for (size_t i = 0; i < stDimension; i++)
	{
		B[i] = 23.0;
	}
#pragma omp for
	for (size_t i = 0; i < stDimension; i++)
	{
		C[i] = 42.0;
	}

	// actual parallel computation starts here
	double t1 = omp_get_wtime();
#pragma omp for
	for (int i = 0; i < stDimension; i++)
	{
		A[i] = B[i] + C[i];
	}
	double t2 = omp_get_wtime() - t1;

// only one thread will print measured computation runtime
#pragma omp single
{
	std::cout << "Computation took " << t2 << " seconds." << std::endl;
}

} // end omp parallel

	delete[] A;
	delete[] B;
	delete[] C;

	return 0;
}
