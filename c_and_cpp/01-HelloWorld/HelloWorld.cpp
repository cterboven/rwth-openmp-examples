/*
 *  OpenMP example collection from RWTH Aachen University
 * 
 *  Written and maintained in 2011-2015 by
 *     Christian Terboven <terboven@itc.rwth-aachen.de>, RWTH Aachen University
 *  and
 *     Dirk Schmidl <schmidl@itc.rwth-aachen.de>, RWTH Aachen University
 *
 *  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
 *  http://creativecommons.org/licenses/by-sa/4.0/
 */

#include <iostream>

#include <omp.h>



int main(int argc, char* argv[])
{

#pragma omp parallel
	{
		std::cout << "Hello World!" << std::endl;
		std::cout << "I am thread " << omp_get_thread_num() << " of " << omp_get_num_threads() << std::endl;
	}

	return 0;
}
